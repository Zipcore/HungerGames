void Menu_ShopLoot(int client)
{
	if(!CanUseShop(client, false, true))
		return;
	
	Menu menu = new Menu(MenuHandler_ShopLoot);
	
	menu.SetTitle("%T", "Shop_LootTitle", client, Stats_GetPoints(client));
	
	char sBuffer[64];
	char sInfo[32];
	
	for (int i = 0; i < g_iLootGroupCount; i++)
	{
		int iPrice;
		
		if(IsPlayerAlive(client))
		{
			if(g_eLoot[i][Loot_ShopPrice] <= 0)
				continue;
			else iPrice = g_eLoot[i][Loot_ShopPrice];
		}
		else if(g_eLoot[i][Loot_ShopPriceDead] <= 0)
			continue;
		else iPrice = g_eLoot[i][Loot_ShopPriceDead];
		
		strcopy(sBuffer, sizeof(sBuffer), g_eLoot[i][Loot_Name]);
		Format(sBuffer, sizeof(sBuffer), "%s (%d)", sBuffer, iPrice);
		
		IntToString(i, sInfo, sizeof(sInfo));
		
		menu.AddItem(sInfo, sBuffer, Stats_GetPoints(client) < iPrice ? ITEMDRAW_DISABLED : ITEMDRAW_DEFAULT);
	}
	
	Format(sBuffer, sizeof(sBuffer), "%T", "Shop_Back", client);
	menu.AddItem("back", sBuffer, ITEMDRAW_DEFAULT);
	
	SetMenuExitButton(menu, true);
	
	menu.Display(client, MENU_TIME_FOREVER);
}

public int MenuHandler_ShopLoot(Menu menu, MenuAction action, int client, int params)
{
	if (action == MenuAction_Select)
	{
		char sInfo[64];
		menu.GetItem(params, sInfo, sizeof(sInfo));
		
		if(StrEqual(sInfo, "back"))
		{
			Menu_Shop(client);
			return;
		}
		
		if(!CanUseShop(client, false, true))
			return;
		
		int iType = StringToInt(sInfo);
		
		int iPrice;
		
		if(IsPlayerAlive(client))
			iPrice = g_eLoot[iType][Loot_ShopPrice];
		else iPrice = g_eLoot[iType][Loot_ShopPriceDead];
		
		if(!IsValidShopTarget(g_iShopTarget[client]))
		{
			CPrintToChat(client, "%T", "Shop_InvalidTarget", client, g_sPrefix);
			return;
		}
		
		if(!Stats_RemovePoints(client, iPrice))
		{
			CPrintToChat(client, "%T", "Shop_NotEnough", client, g_sPrefix, iPrice, Stats_GetPoints(client));
			return;
		}
			
		g_iShopLastTarget[client] = g_iShopTarget[client];
		
		float fPos[3];
		GetClientEyePosition(g_iShopTarget[client], fPos);
		
		float fAngle[3];
		GetClientEyeAngles(g_iShopTarget[client], fAngle);
		
		float fFinal[3];
		AddInFrontOf(fPos, fAngle, 100.0, fFinal);
		
		ZCore_LootSpawner_ForceSpawn(iType, fFinal, g_iShopTarget[client], LS_ACTION_SPAWN);
	}
	else if (action == MenuAction_End)
		delete menu;
}