void Menu_ShopEvents(int client)
{
	if(!CanUseShop(client, true, true))
		return;
	
	Menu menu = new Menu(MenuHandler_ShopEvents);
	
	menu.SetTitle("%T", "Shop_EventsTitle", client, Stats_GetPoints(client));
	
	char sBuffer[64];
	
	if(g_iShopEventPriceIonCannon > 0)
	{
		Format(sBuffer, sizeof(sBuffer), "%T (%d)", "Event_IonCannon", client, g_iShopEventPriceIonCannon);
		menu.AddItem("event_ion", sBuffer, Stats_GetPoints(client) < g_iShopEventPriceIonCannon ? ITEMDRAW_DISABLED : ITEMDRAW_DEFAULT);
	}
	
	if(g_iShopEventPriceArtillery > 0)
	{
		Format(sBuffer, sizeof(sBuffer), "%T (%d)", "Event_Artillery", client, g_iShopEventPriceArtillery);
		menu.AddItem("event_artillery", sBuffer, Stats_GetPoints(client) < g_iShopEventPriceArtillery ? ITEMDRAW_DISABLED : ITEMDRAW_DEFAULT);
	}
	
	if(g_iShopEventPriceNapalm > 0)
	{
		Format(sBuffer, sizeof(sBuffer), "%T (%d)", "Event_Napalm", client, g_iShopEventPriceNapalm);
		menu.AddItem("event_napalm", sBuffer, Stats_GetPoints(client) < g_iShopEventPriceNapalm ? ITEMDRAW_DISABLED : ITEMDRAW_DEFAULT);
	}
	
	if(g_iShopEventPriceThunderStorm > 0)
	{
		Format(sBuffer, sizeof(sBuffer), "%T (%d)", "Event_ThunderStorm", client, g_iShopEventPriceThunderStorm);
		menu.AddItem("event_thunder", sBuffer, Stats_GetPoints(client) < g_iShopEventPriceThunderStorm ? ITEMDRAW_DISABLED : ITEMDRAW_DEFAULT);
	}
	
	if(g_iShopEventPriceChargedWater > 0)
	{
		Format(sBuffer, sizeof(sBuffer), "%T (%d)", "Event_ChargedWater", client, g_iShopEventPriceChargedWater);
		menu.AddItem("event_chargedwater", sBuffer, Stats_GetPoints(client) < g_iShopEventPriceChargedWater ? ITEMDRAW_DISABLED : ITEMDRAW_DEFAULT);
	}
	
	if(g_iShopEventPriceEarthquake > 0)
	{
		Format(sBuffer, sizeof(sBuffer), "%T (%d)", "Event_Earthquake", client, g_iShopEventPriceEarthquake);
		menu.AddItem("event_earthquake", sBuffer, Stats_GetPoints(client) < g_iShopEventPriceEarthquake ? ITEMDRAW_DISABLED : ITEMDRAW_DEFAULT);
	}
	
	Format(sBuffer, sizeof(sBuffer), "%T", "Shop_Back", client);
	menu.AddItem("back", sBuffer, ITEMDRAW_DEFAULT);
	
	SetMenuExitButton(menu, true);
	
	menu.Display(client, MENU_TIME_FOREVER);
}

public int MenuHandler_ShopEvents(Menu menu, MenuAction action, int client, int params)
{
	if (action == MenuAction_Select)
	{
		if(!CanUseShop(client, true, true))
			return;
		
		char sInfo[64];
		menu.GetItem(params, sInfo, sizeof(sInfo));
		
		int iEvent = EV_None;
		int iPrice = 0;
		
		if(StrEqual(sInfo, "event_ion"))
		{
			iEvent = EV_IonCannon;
			iPrice = g_iShopEventPriceIonCannon;
		}
		else if(StrEqual(sInfo, "event_artillery"))
		{
			iEvent = EV_Artillery;
			iPrice = g_iShopEventPriceArtillery;
		}
		else if(StrEqual(sInfo, "event_napalm"))
		{
			iEvent = EV_Napalm;
			iPrice = g_iShopEventPriceNapalm;
		}
		else if(StrEqual(sInfo, "event_thunder"))
		{
			iEvent = EV_ThunderStorm;
			iPrice = g_iShopEventPriceThunderStorm;
		}
		else if(StrEqual(sInfo, "event_chargedwater"))
		{
			iEvent = EV_ChargedWater;
			iPrice = g_iShopEventPriceChargedWater;
		}
		else if(StrEqual(sInfo, "event_earthquake"))
		{
			iEvent = EV_Earthquake;
			iPrice = g_iShopEventPriceEarthquake;
		}
		
		if(iEvent != EV_None)
		{
			int iTarget = GetShopTarget(client)
			
			if(!IsValidShopTarget(g_iShopTarget[client]))
			{
				CPrintToChat(client, "%T", "Shop_InvalidTarget", client, g_sPrefix);
				Menu_Shop(client);
				return;
			}
			
			if(iTarget == client)
			{
				CPrintToChat(client, "%T", "Shop_InvalidTargetEventSelf", client, g_sPrefix);
				Menu_Shop(client);
				return;
			}
			
			if(!Stats_RemovePoints(client, iPrice))
			{
				CPrintToChat(client, "%T", "Shop_NotEnough", client, g_sPrefix, iPrice, Stats_GetPoints(client));
				Menu_Shop(client);
				return;
			}
			
			g_iShopLastTarget[client] = g_iShopTarget[client];
			
			g_iLastEvent = GetTime();
			StartNewEvent(iEvent, iTarget);
		}
		else Menu_Shop(client);
	}
	else if (action == MenuAction_End)
		delete menu;
}