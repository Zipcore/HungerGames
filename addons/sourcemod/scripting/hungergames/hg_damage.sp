public Action OnWeaponCanUse(int client, int weapon)
{
	char sWeapon[32];
	GetEntityClassname(weapon, sWeapon, sizeof(sWeapon));
	
	// Zombies can only pickup knifes
	if(IsZombie(client))
	{
		if(StrContains(sWeapon, "knife", false) != -1 || StrContains(sWeapon, "bayonet") != -1)
			return Plugin_Continue;
		else return Plugin_Handled;
	}
	
	// Knocked out player can't pickup weapons
	if(IsKnockedOut(client) && StrContains(sWeapon, "knife", false) != -1 || StrContains(sWeapon, "bayonet") != -1)
		return Plugin_Handled;
		
	// Tributes can pickup any weapon
	if(g_bIsTribute[client])
		return Plugin_Continue;
	
	return Plugin_Handled;
}

public Action OnTakeDamage(int victim, int &attacker, int &inflictor, float &damage, int &damagetype, int &weapon, float damageForce[3], float damagePosition[3])
{
	if(attacker == 0)
		return Plugin_Continue;
		
	if(!Entity_IsPlayer(attacker))
		return Plugin_Continue;
	
	// Zombies can't attack each other
	
	if (IsZombie(victim) && IsZombie(attacker))
		return Plugin_Handled;
	
	// Round is over, don't allow players to die now
	
	if(RoundStatus == RS_DelayedRoundEnd)
		return Plugin_Handled;
	
	// If player is knocked out we only like to allow damage done to his ragdoll or damage by non-player sources
	
	if(IsKnockedOut(victim) && weapon > 0 && (attacker != 0 || inflictor != 0))
		return Plugin_Handled;
	
	// Balance team damage during the round, not endgame
	
	float damageOld = damage;
	
	if(RoundStatus == RS_InProgress)
	{
		int vparty = GetParty(victim);
		int aparty = GetParty(attacker);
		
		// Party member attacks single player
		if(vparty == -1 && vparty != -1)
		{
			damage *= g_fPartyDmgPartyAttacksSingle;
		}
		
		// Single player attacks party
		else if(vparty != -1 && vparty == -1)
		{
			damage *= g_fPartyDmgSingleAttacksParty;
		}
		
		// Party attacks party
		else if(vparty != -1 && vparty != -1)
		{
			// Same party as attacker
			if(vparty == aparty)
				damage *= g_fPartyDmgMateAttacksMate
			else damage *= g_fPartyDmgPartyAttacksParty;
		}
	}
	
	// Knockout
	
	int iHealth = GetClientHealth(victim);
	
	if(iHealth > damage && damage >= g_iKnockoutDamage && g_fKnockoutChance > 0.0)
	{
		if(GetRandomFloat(0.0, 1.0) <= g_fKnockoutChance)
			KnockoutPlayer(victim);
	}
	
	if(damageOld != damage)
		return Plugin_Changed;
	
	return Plugin_Continue;
}