Handle g_hGiveCompassTimer[MAXPLAYERS+1] = {null, ...};
bool g_bCompass[MAXPLAYERS+1]; //Shows next player direction
bool g_bJammer[MAXPLAYERS+1]; //Excludes you from compass
bool g_bTracer[MAXPLAYERS+1]; //Same as compass but shows player name too
bool g_bGPS[MAXPLAYERS+1]; //Re-Enables the radar, jammer does not protect (not possible anyway)

void GiveCompassDelayed(int client)
{
	ResetGiveCompassTimer(client);
	
	if (g_fGiveCompassDelay >= 0.0)
		g_hGiveCompassTimer[client] = CreateTimer(g_fGiveCompassDelay, Timer_GiveCompass, client);
}

void ResetGiveCompassTimer(int client)
{
	if(g_hGiveCompassTimer[client] != null)
	{
		CloseHandle(g_hGiveCompassTimer[client]);
		g_hGiveCompassTimer[client] = null;
	}
}

public Action Timer_GiveCompass(Handle timer, any client)
{
	GiveCompass(client, true);
	g_hGiveCompassTimer[client] = null;
	
	DeleteCompassLoot();
	
	return Plugin_Handled;
}

void GiveCompass(int client, bool msg = false)
{
	if(!IsClientInGame(client))
		return;
		
	if(!IsPlayerAlive(client))
		return;
		
	if(GetClientTeam(client) <= CS_TEAM_SPECTATOR)
		return;
		
	if(!g_bIsTribute[client])
		return;
	
	if(HasCompass(client))
		return;
		
	g_bCompass[client] = true;
	
	if(msg)
		CPrintToChat(client, "%T", "GotCompass", client, g_sPrefix);
}

bool HasCompass(int client)
{
	return g_bCompass[client];
}

void ResetCompass(int client)
{
	g_bCompass[client] = false;
}

void GiveJammer(int client)
{
	if(!IsClientInGame(client))
		return;
		
	if(!IsPlayerAlive(client))
		return;
		
	if(GetClientTeam(client) <= CS_TEAM_SPECTATOR)
		return;
		
	if(!g_bIsTribute[client])
		return;
	
	if(HasJammer(client))
		return;
		
	g_bJammer[client] = true;
}

bool HasJammer(int client)
{
	return g_bJammer[client];
}

void ResetJammer(int client)
{
	g_bJammer[client] = false;
}

void GiveTracer(int client)
{
	if(!IsClientInGame(client))
		return;
		
	if(!IsPlayerAlive(client))
		return;
		
	if(GetClientTeam(client) <= CS_TEAM_SPECTATOR)
		return;
		
	if(!g_bIsTribute[client])
		return;
	
	if(HasTracer(client))
		return;
		
	g_bTracer[client] = true;
}

bool HasTracer(int client)
{
	return g_bTracer[client];
}

void ResetTracer(int client)
{
	g_bTracer[client] = false;
}

void GiveGPS(int client)
{
	if(!IsClientInGame(client))
		return;
		
	if(!IsPlayerAlive(client))
		return;
		
	if(GetClientTeam(client) <= CS_TEAM_SPECTATOR)
		return;
		
	if(!g_bIsTribute[client])
		return;
	
	if(HasGPS(client))
		return;
		
	g_bGPS[client] = true;
	
	ShowRadar(client);
}

bool HasGPS(int client)
{
	return g_bGPS[client];
}

void ResetGPS(int client)
{
	g_bGPS[client] = false;
}

int GetCompassInfo(int client, char sInfo[32], char sName[64])
{
	float clientOrigin[3];
	GetClientAbsOrigin(client, clientOrigin);
	
	int nearest = GetCompassTarget(client, clientOrigin);
	
	if(nearest <= 0)
		return false;
	
	float targetOrigin[3];
	GetClientAbsOrigin(nearest, targetOrigin);
	
	float clientAngles[3];
	GetClientAbsAngles(client, clientAngles);
	
	float vecPoints[3];
	MakeVectorFromPoints(clientOrigin, targetOrigin, vecPoints);
	
	float vecAngles[3];
	GetVectorAngles(vecPoints, vecAngles);
	
	float distance;
	distance = GetVectorDistance(clientOrigin, targetOrigin);
	
	// Get direction
	float diff = clientAngles[1] - vecAngles[1];
	
	while(diff < -180.0)
		diff += 360.0;
	
	while(diff > 180.0)
		diff -= 360.0;
	
	// up
	if (diff >= -22.5 && diff < 22.5)
	{
		Format(sInfo, sizeof(sInfo), "\xe2\x86\x91");
	}
	
	// right up
	else if (diff >= 22.5 && diff < 67.5)
	{
		if(distance > g_fCompassDisorientationDistance)
			Format(sInfo, sizeof(sInfo), "\xe2\x86\x97");
		// up
		else Format(sInfo, sizeof(sInfo), "\xe2\x86\x91");
	}
	
	// right
	else if (diff >= 67.5 && diff < 112.5)
	{
		Format(sInfo, sizeof(sInfo), "\xe2\x86\x92");
	}
	
	// right down
	else if (diff >= 112.5 && diff < 157.5)
	{
		if(distance > g_fCompassDisorientationDistance)
			Format(sInfo, sizeof(sInfo), "\xe2\x86\x98");
		// right
		else Format(sInfo, sizeof(sInfo), "\xe2\x86\x92");
	}
	
	// down
	else if (diff >= 157.5 || diff < -157.5)
	{
		Format(sInfo, sizeof(sInfo), "\xe2\x86\x93");
	}

	// down left
	else if (diff >= -157.5 && diff < -112.5)
	{
		if(distance > g_fCompassDisorientationDistance)
			Format(sInfo, sizeof(sInfo), "\xe2\x86\x99");
		// down
		else Format(sInfo, sizeof(sInfo), "\xe2\x86\x93");
	}
	
	// left
	else if (diff >= -112.5 && diff < -67.5)
	{
		Format(sInfo, sizeof(sInfo), "\xe2\x86\x90");
	}
	
	// left up
	else 
	{
		if(distance > g_fCompassDisorientationDistance)
			Format(sInfo, sizeof(sInfo), "\xe2\x86\x96");
		// left
		else Format(sInfo, sizeof(sInfo), "\xe2\x86\x90");
	}
	
	char sTarget[12];
	GetClientName(nearest, sTarget, sizeof(sTarget));
	
	if(IsZombie(client))
	{
		int iMeters = RoundToFloor(distance * 0.02);
		Format(sName, sizeof(sName), "~%dm", iMeters);
	}
	else if(HasTracer(client))
	{
		int iMeters = RoundToFloor(distance * 0.02);
		Format(sName, sizeof(sName), "%s ( ~%dm )", sTarget, iMeters);
	}
	else if(distance < g_fCompassShowNameDistance)
	{
		Format(sName, sizeof(sName), "%s", sTarget);
	}
	
	return true;
}

int GetCompassTarget(int client, float pos[3])
{
	float fDistance;
	float fClosestDistance = -1.0;
	
	int player = 0;
	int party = GetParty(client);
	
	LoopAlivePlayers(i)
	{
		if(i == client)
			continue;
			
		if(!g_bIsTribute[i])
			continue;
			
		if(party != -1 && party == GetParty(i))
			continue;
			
		if(HasJammer(i))
			continue;
			
		if(IsDelayedDeath(client))
			continue;
		
		float fTargetPos[3];
		GetClientAbsOrigin(i, fTargetPos);
		
		if(fTargetPos[0] == 0.0 && fTargetPos[1] == 0.0 && fTargetPos[2] == 0.0)
			continue;
		
		fDistance = GetVectorDistance(pos, fTargetPos);
		
		if (fDistance < fClosestDistance || fClosestDistance == -1.0)
		{
			fClosestDistance = fDistance;
			player = i;
		}
	}
	
	return player;
}