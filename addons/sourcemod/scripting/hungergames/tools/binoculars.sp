bool g_bBinoculars[MAXPLAYERS+1];
bool g_bBinocularsEnabled[MAXPLAYERS+1];
int g_iDefaultFOV[MAXPLAYERS+1];
Handle g_hGiveBinocularsTimer[MAXPLAYERS+1] = {null, ...};

void GiveBinocularsDelayed(int client)
{
	ResetGiveBinocularsTimer(client);
	
	if (g_fGiveBinocularsDelay >= 0.0)
		g_hGiveBinocularsTimer[client] = CreateTimer(g_fGiveBinocularsDelay, Timer_GiveBinoculars, client);
}

void ResetGiveBinocularsTimer(int client)
{
	if(g_hGiveBinocularsTimer[client] != null)
	{
		CloseHandle(g_hGiveBinocularsTimer[client]);
		g_hGiveBinocularsTimer[client] = null;
	}
}

public Action Timer_GiveBinoculars(Handle timer, any client)
{
	GiveBinoculars(client, true);
	g_hGiveBinocularsTimer[client] = null;
	
	DeleteBinocularLoot();
	
	return Plugin_Handled;
}

void GiveBinoculars(int client, bool msg = false)
{
	if(!IsClientInGame(client))
		return;
		
	if(!IsPlayerAlive(client))
		return;
		
	if(GetClientTeam(client) <= CS_TEAM_SPECTATOR)
		return;
		
	if(!g_bIsTribute[client])
		return;
	
	if(HasBinoculars(client))
		return;
		
	g_bBinoculars[client] = true;
	
	if(msg)
		CPrintToChat(client, "%T", "GotBinoculars", client, g_sPrefix);
}

bool HasBinoculars(int client)
{
	return g_bBinoculars[client];
}

bool HasBinocularsEnabled(int client)
{
	return g_bBinocularsEnabled[client];
}

void ResetBinoculars(int client)
{
	if(IsClientInGame(client))
		g_iDefaultFOV[client] = GetEntProp(client, Prop_Send, "m_iFOV");
	else g_iDefaultFOV[client] = 90;
		
	g_bBinoculars[client] = false;
	g_bBinocularsEnabled[client] = false;
}

void CheckBinoculars(int client, int buttons, int weapon)
{
	if(HasBinoculars(client))
	{
		// Toggle binoculars
		if((GetEntityFlags(client) & FL_ONGROUND) && (buttons & IN_USE) && !HasBinocularsEnabled(client))
			EnableBinoculars(client);
		else if(!(GetEntityFlags(client) & FL_ONGROUND) || !(buttons & IN_USE) && HasBinocularsEnabled(client))
			DisableBinoculars(client);
		
		// Prevent shooting with binoculars
		if(HasBinocularsEnabled(client))
			BlockWeapon(client, weapon, 1.0);
	}
}

void EnableBinoculars(int client)
{	
	SetEntProp(client, Prop_Send, "m_iFOV", 15);
	g_bBinocularsEnabled[client] = true;
}

void DisableBinoculars(int client)
{
	SetEntProp(client, Prop_Send, "m_iFOV", g_iDefaultFOV[client]);
	g_bBinocularsEnabled[client] = false;
}