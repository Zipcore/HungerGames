// Credits: Bacardi

Handle g_aThrownKnives = null;
Handle g_hTimerDelay[MAXPLAYERS+1] = {null, ...};
bool g_bHeadshot[MAXPLAYERS+1];
int g_iPlayerKniveCount[MAXPLAYERS+1];

void ClearThrowingKnifes()
{
	if(g_aThrownKnives == null)
		g_aThrownKnives = CreateArray(1);
	else ClearArray(g_aThrownKnives);
}

void ResetThrowingKnifes(int client)
{
	g_iPlayerKniveCount[client] = 0;
}

int GetThrowingKnifes(int client)
{
	return g_iPlayerKniveCount[client];
}

void AddThrowingKnife(int client)
{
	g_iPlayerKniveCount[client]++;
}

void ThrowKnife(int client)
{
	if(g_iPlayerKniveCount[client] <= 0)
		return;
		
	if(g_hTimerDelay[client] != null)
		return;
		
	if(g_aThrownKnives == null)
		g_aThrownKnives = CreateArray(1);
	
	g_hTimerDelay[client] = CreateTimer(0.0, Timer_CreateKnife, client);
}

public Action Timer_CreateKnife(Handle timer, any client)
{
	g_hTimerDelay[client] = null;
	
	if(!client || !IsClientInGame(client) || !IsPlayerAlive(client))
		return Plugin_Stop;

	int slot_knife = GetPlayerWeaponSlot(client, CS_SLOT_KNIFE);
	int knife = CreateEntityByName("smokegrenade_projectile");

	if(knife == -1 || !DispatchSpawn(knife))
		return Plugin_Stop;

	// owner
	int team = GetClientTeam(client);
	SetEntPropEnt(knife, Prop_Send, "m_hOwnerEntity", client);
	SetEntPropEnt(knife, Prop_Send, "m_hThrower", client);
	SetEntProp(knife, Prop_Send, "m_iTeamNum", team);

	// player knife model
	char model[PLATFORM_MAX_PATH];
	if(slot_knife != -1)
	{
		GetEntPropString(slot_knife, Prop_Data, "m_ModelName", model, sizeof(model));
		if(ReplaceString(model, sizeof(model), "v_knife_", "w_knife_", true) != 1)
			model[0] = '\0';
		else if(ReplaceString(model, sizeof(model), ".mdl", "_dropped.mdl", true) != 1)
			model[0] = '\0';
	}

	if(!FileExists(model, true))
		Format(model, sizeof(model), "%s", team == CS_TEAM_T ? "models/weapons/w_knife_default_t_dropped.mdl":"models/weapons/w_knife_default_ct_dropped.mdl");

	// model and size
	SetEntProp(knife, Prop_Send, "m_nModelIndex", PrecacheModel(model));
	SetEntPropFloat(knife, Prop_Send, "m_flModelScale", 1.0);
	SetEntPropFloat(knife, Prop_Send, "m_flElasticity", 0.2);
	SetEntPropFloat(knife, Prop_Data, "m_flGravity", 1.0);

	// Player origin and angle
	float origin[3];
	float angle[3];
	GetClientEyePosition(client, origin);
	GetClientEyeAngles(client, angle);

	// knive new spawn position and angle is same as player's
	float pos[3];
	GetAngleVectors(angle, pos, NULL_VECTOR, NULL_VECTOR);
	ScaleVector(pos, 50.0);
	AddVectors(pos, origin, pos);

	// knive flying direction and speed/power
	float player_velocity[3];
	float velocity[3];
	GetEntPropVector(client, Prop_Data, "m_vecVelocity", player_velocity);
	GetAngleVectors(angle, velocity, NULL_VECTOR, NULL_VECTOR);
	ScaleVector(velocity, 2250.0);
	AddVectors(velocity, player_velocity, velocity);

	// spin knive
	float spin[] = {4000.0, 0.0, 0.0};
	SetEntPropVector(knife, Prop_Data, "m_vecAngVelocity", spin);

	// Stop grenade detonate and Kill knive after 1 - 30 sec
	SetEntProp(knife, Prop_Data, "m_nNextThinkTick", -1);
	char buffer[25];
	Format(buffer, sizeof(buffer), "!self,Kill,,%0.1f,-1", 1.5);
	DispatchKeyValue(knife, "OnUser1", buffer);
	AcceptEntityInput(knife, "FireUser1");

	// Throw knive!
	TeleportEntity(knife, pos, angle, velocity);
	SDKHookEx(knife, SDKHook_Touch, KnifeHit);

	PushArrayCell(g_aThrownKnives, EntIndexToEntRef(knife));
	g_iPlayerKniveCount[client]--;
	
	return Plugin_Stop;
}

public Action KnifeHit(int knife, int other)
{
	if(0 < other <= MaxClients) // Hits player index
	{
		int victim = other;

		SetVariantString("csblood");
		AcceptEntityInput(knife, "DispatchEffect");
		AcceptEntityInput(knife, "Kill");

		int attacker = GetEntPropEnt(knife, Prop_Send, "m_hThrower");
		int inflictor = GetPlayerWeaponSlot(attacker, CS_SLOT_KNIFE);

		if(inflictor == -1)
			inflictor = attacker;

		float victimeye[3];
		GetClientEyePosition(victim, victimeye);

		float damagePosition[3];
		float damageForce[3];

		GetEntPropVector(knife, Prop_Data, "m_vecOrigin", damagePosition);
		GetEntPropVector(knife, Prop_Data, "m_vecVelocity", damageForce);

		if(GetVectorLength(damageForce) == 0.0) // knife movement stop
			return;

		// Headshot - shitty way check it, clienteyeposition almost player back...
		float distance = GetVectorDistance(damagePosition, victimeye);
		g_bHeadshot[attacker] = distance <= 20.0;

		// damage values and type
		float damage[2];
		damage[0] = float(g_iThrowingKnifesDamage);
		damage[1] = float(g_iThrowingKnifesDamageHeadshot);
		int dmgtype = DMG_SLASH|DMG_NEVERGIB;

		if(g_bHeadshot[attacker])
			dmgtype |= DMG_HEADSHOT;

		// create damage
		SDKHooks_TakeDamage(victim, inflictor, attacker,
		g_bHeadshot[attacker] ? damage[1]:damage[0],
		dmgtype, knife, damageForce, damagePosition);

		// blood effect
		int color[] = {255, 0, 0, 255};
		float dir[3];

		TE_SetupBloodSprite(damagePosition, dir, color, 1, PrecacheDecal("sprites/blood.vmt"), PrecacheDecal("sprites/blood.vmt"));
		TE_SendToAll(0.0);

		// ragdoll effect
		int ragdoll = GetEntPropEnt(victim, Prop_Send, "m_hRagdoll");
		if(ragdoll != -1)
		{
			ScaleVector(damageForce, 50.0);
			damageForce[2] = FloatAbs(damageForce[2]); // push up!
			SetEntPropVector(ragdoll, Prop_Send, "m_vecForce", damageForce);
			SetEntPropVector(ragdoll, Prop_Send, "m_vecRagdollVelocity", damageForce);
		}
	}
	else if(FindValueInArray(g_aThrownKnives, EntIndexToEntRef(other)) != -1) // knives collide
	{
		SDKUnhook(knife, SDKHook_Touch, KnifeHit);
		
		float pos[3];
		float dir[3];
		
		GetEntPropVector(knife, Prop_Data, "m_vecOrigin", pos);
		
		TE_SetupArmorRicochet(pos, dir);
		TE_SendToAll(0.0);

		DispatchKeyValue(knife, "OnUser1", "!self,Kill,,1.0,-1");
		AcceptEntityInput(knife, "FireUser1");
	}
}

public void OnEntityDestroyed(int entity)
{
	if(!IsValidEdict(entity))
		return;
		
	if(g_aThrownKnives == null)
		return;

	int index = FindValueInArray(g_aThrownKnives, EntIndexToEntRef(entity));
	if(index != -1) RemoveFromArray(g_aThrownKnives, index);
}