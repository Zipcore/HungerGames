float g_fEndgameStart;

float g_fArenaRadiusSteps = 16.0;
float g_fArenaInterval = 0.5;

int g_iSpawnCount;
float g_fSpawnPos[128][3]; // 128... stupid mappers
bool g_bSpawnUsed[128];

float g_fArenaCenter[3];
float g_fArenaRadius;
float g_fArenaStartRadius;

bool g_bFirstbeacon;

bool g_bFinal;

bool IsFinal()
{
	return g_bFinal;
}

void StartEndgame()
{
	LoopTributes(i)
	{
		int iPoints = g_iPointsEndgame;
		
		if(Client_HasAdminFlags(i, g_iVIPflags))
			iPoints = RoundToFloor(float(iPoints)*g_fPointsVIP);
		
		Stats_AddPoints(i, iPoints, true);
		
		if(g_iPartyResetEndgame == 2)
			LeaveParty(i);
		else if(g_iPartyResetEndgame == 1)
			ResetPartyIcon(i);
	}
	
	g_fEndgameStart = GetGameTime();
	g_bFirstbeacon = true;
    
	if(g_fBeaconInterval > 0.0)
		SetupBeacon();
	else if(g_fKnifeFightDelay != -1.0)
		SetupArena();
		
	if(g_bEndgameWipeLoot)
	{
		ZCore_LootSpawner_WipeEntitysAll();
		CleanupWeapons();
	}
}

void SetupBeacon()
{
	CreateTimer(g_fBeaconInterval, Timer_Beacon, _, TIMER_REPEAT);
}

public Action Timer_Beacon(Handle timer, any data)
{
	// New round started
	if(RoundStatus != RS_Endgame)
		return Plugin_Stop;
	
	if(g_bFirstbeacon)
	{
		g_bFirstbeacon = false;
		return Plugin_Continue;
	}
	
	// Beacon is not enough
	if(g_fKnifeFightDelay > 0.0 && GetGameTime()-g_fEndgameStart > g_fKnifeFightDelay)
	{
		LoopTributes(i)
		{
			int iPoints = g_iPointsFinalKnifefight;
			
			if(Client_HasAdminFlags(i, g_iVIPflags))
				iPoints = RoundToFloor(float(iPoints)*g_fPointsVIP);
			
			Stats_AddPoints(i, iPoints, true);
		}
		
		SetupArena();
		return Plugin_Stop;
	}
	
	CreateBeacons();
	
	return Plugin_Continue;
}

void CreateBeacons()
{
	LoopAlivePlayers(i)
	{
		if(!g_bIsTribute[i])
			continue;
			
		float fPos[3];
		GetClientAbsOrigin(i, fPos);
		fPos[2] += 8;
		
		TE_SetupBeamRingPoint(fPos, 10.0, 750.0, g_iBombRing, g_iHalo, 0, 10, 0.6, 10.0, 0.5, {255, 75, 75, 255}, 5, 0);
		TE_SendToAll();
		
		EmitAmbientSoundAny(sndBeacon, fPos);
	}
}

void SetupArena()
{
	LoopIngamePlayers(i)
	{
		CPrintToChat(i, "%T", "KnifeFight_Started", i, g_sPrefix);
	}
	
	if(g_bSndKnifeFight)
		EmitSoundToAllAny(sndKnifeFight);
	
	GetSpawnsAndCenter();
	
	LoopTributes(i)
	{
		bool teleported;
		while(!teleported)
		{
			int random = GetRandomInt(0, g_iSpawnCount - 1);
			if(!g_bSpawnUsed[random])
			{	
				HG_ResetHunger(i);
				HG_ResetEnergy(i);
				HG_ResetThirst(i);
				HG_ResetStamina(i);
				UpdateSpeed(i);
				
				ResetTools(i);
				SetEntityHealth(i, 100);
				
				StripPlayerWeapons(i, true, false);
				ResetThrowingKnifes(i);
				
				// Force player to look at the center
				float angles[3]; 
				float resultant[3];
				MakeVectorFromPoints(g_fArenaCenter, g_fSpawnPos[random], resultant);
				GetVectorAngles(resultant, angles);
				
				if (angles[0] >= 270)
				{
					angles[0] -= 270;
					angles[0] = (90 - angles[0]);
				}
				else if (angles[0] <= 90)
				{
					angles[0] *= -1;
				}
				angles[1] -= 180;
				
				float fNull[3];
				TeleportEntity(i, g_fSpawnPos[random], angles, fNull);
				
				GiveTributeKnife(i);
				
				g_bSpawnUsed[random] = true;
				teleported = true;
			}
		}
	}
	
	CleanupWeapons();
	CreateTimer(g_fArenaInterval, Timer_Arena, _, TIMER_REPEAT);
	g_bFinal = true;
	
	/* Empty Loot crates */
	EmptyLoot();
	ZCore_LootSpawner_WipeEntitysAll();
	
	/* Slay non-Tributes */
	LoopAlivePlayers(i)
	{
		if(g_bIsTribute[i])
			continue;
			
		ForcePlayerSuicide(i);
	}
}

public Action Timer_Arena(Handle timer, any data)
{
	// New round started
	if(RoundStatus != RS_Endgame)
	{
		g_bFinal = false;
		return Plugin_Stop;
	}
	
	CheckArena();
	
	return Plugin_Continue;
}

void CheckArena()
{	
	if(g_fArenaRadius <= 0.0)
	{
		LoopAlivePlayers(i)
			ForcePlayerSuicide(i);
		
		return;
	}
	
	CreateAreneaBeams();
		
	LoopAlivePlayers(i)
	{
		if(Entity_GetDistanceOrigin(i, g_fArenaCenter) < (g_fArenaRadius/2.0))
			continue;
			
		Point_Hurt(i, 20, 0, DMG_DROWN);
	}
	
	// Shrink area
	g_fArenaRadius -= g_fArenaRadiusSteps;
}

void GetSpawnsAndCenter()
{
	g_fArenaCenter[0] = 0.0;
	g_fArenaCenter[1] = 0.0;
	g_fArenaCenter[2] = 0.0;
	
	g_iSpawnCount = 0;
	
	if (!LoadArenaCenter())
		UseMapSpawns();
	else
	{
		int iAlive;
		
		LoopTributes(i)
			iAlive++;
		
		float fStepsSize = 360.0/float(iAlive);
		
		for (int i = 0; i < iAlive; i++)
		{
			GetCircuitPos(g_fArenaCenter, (g_fArenaStartRadius-128.0)/2.0, fStepsSize*float(i), g_fSpawnPos[g_iSpawnCount], false, true);
			g_bSpawnUsed[g_iSpawnCount] = false;
			
			g_iSpawnCount++;
		}
		
		g_fArenaRadius = g_fArenaStartRadius;
	}
}

void UseMapSpawns()
{
	int	entity;
	while ((entity = FindEntityByClassname(entity, "info_player_counterterrorist" )) != -1)
	{
		Entity_GetAbsOrigin(entity, g_fSpawnPos[g_iSpawnCount]);
		AddVectors(g_fArenaCenter, g_fSpawnPos[g_iSpawnCount], g_fArenaCenter);
		g_bSpawnUsed[g_iSpawnCount] = false;
		
		g_iSpawnCount++;
	}
	
	while ((entity = FindEntityByClassname(entity, "info_player_terrorist" )) != -1)
	{
		Entity_GetAbsOrigin(entity, g_fSpawnPos[g_iSpawnCount]);
		AddVectors(g_fArenaCenter, g_fSpawnPos[g_iSpawnCount], g_fArenaCenter);
		g_bSpawnUsed[g_iSpawnCount] = false;
		
		g_iSpawnCount++;
	}
	
	g_fArenaCenter[0] /= g_iSpawnCount;
	g_fArenaCenter[1] /= g_iSpawnCount;
	g_fArenaCenter[2] /= g_iSpawnCount;
		
	g_fArenaRadius = (GetVectorDistance(g_fSpawnPos[0], g_fArenaCenter) * 2.0) + 128.0;
	g_fArenaStartRadius = g_fArenaRadius;
}

bool LoadArenaCenter()
{
	char sRawMap[PLATFORM_MAX_PATH];
	char sMap[64];
	GetCurrentMap(sRawMap, sizeof(sRawMap));
	RemoveMapPath(sRawMap, sMap, sizeof(sMap));
	
	char sPath[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, sPath, sizeof(sPath), "configs/hg/knife_arena/%s.txt", sMap);
	
	Handle hFile = OpenFile(sPath, "r");
	
	char buffer[512];
	char bufferExp[4][32];
	
	if (hFile != INVALID_HANDLE)
	{
		while (ReadFileLine(hFile, buffer, sizeof(buffer))) 
		{
			ExplodeString(buffer, ";", bufferExp, 4, 32);
			
			g_fArenaCenter[0] = StringToFloat(bufferExp[0]);
			g_fArenaCenter[1] = StringToFloat(bufferExp[1]);
			g_fArenaCenter[2] = StringToFloat(bufferExp[2]);
			g_fArenaStartRadius = StringToFloat(bufferExp[3]);
			g_fArenaRadius = g_fArenaStartRadius;
			
			return true;
		}
		
		CloseHandle(hFile);
	}
	
	return false;
}

bool g_bSetKnifeArena[MAXPLAYERS+1];
bool g_bSetKnifeArenaRadius[MAXPLAYERS+1];

public Action Cmd_Set_KnifeArena(int client, int args)
{
	if(client == 0)
		return Plugin_Handled;
		
	g_bSetKnifeArena[client] = true;
	
	CPrintToChat(client, "%s Press and hold MOUSE2 to set the knife arena.", g_sPrefix);
	
	return Plugin_Handled;
}

float g_fPreviewBlocktime[MAXPLAYERS + 1];

void AdminSetKnifeArena(int client, int buttons)
{
	if(!g_bSetKnifeArena[client])
		return;
		
	if(buttons & IN_ATTACK)
	{
		g_bSetKnifeArena[client] = false;
		g_bSetKnifeArenaRadius[client] = false;
	}
	else if(buttons & IN_ATTACK2)
	{
		float fTime = GetGameTime();
		
		if(!g_bSetKnifeArenaRadius[client])
		{
			GetClientAimTargetPos(client, g_fArenaCenter);
			g_bSetKnifeArenaRadius[client] = true;
			g_fPreviewBlocktime[client] = fTime;
		}
		
		float fPointRadius[3];
		GetClientAimTargetPos(client, fPointRadius);
		fPointRadius[2] += 5;
		
		g_fArenaRadius = 2.0*GetVectorDistance(fPointRadius, g_fArenaCenter)
	
		if(g_fArenaRadius > 2048.0)
			g_fArenaRadius = 2048.0;
		else if(g_fArenaRadius < 384.0)
			g_fArenaRadius = 384.0;
			
		g_fArenaStartRadius = g_fArenaRadius;
		
		if(g_fPreviewBlocktime[client] <= fTime)
		{
			CreateAreneaBeams(true);
			g_fPreviewBlocktime[client] = fTime+0.1;
		}
	}
	else if(g_bSetKnifeArenaRadius[client])
	{
		g_bSetKnifeArena[client] = false;
		g_bSetKnifeArenaRadius[client] = false;
		
		if(SaveKnifeArena())
			CPrintToChat(client, "%s Knife arena center has been set to your current position.", g_sPrefix);
		else CPrintToChat(client, "%s Can't save to file. configs/hg/knife_arena/ ensure this path has write permissions.", g_sPrefix);
	}
}

bool SaveKnifeArena()
{
	char sRawMap[PLATFORM_MAX_PATH];
	char sMap[64];
	GetCurrentMap(sRawMap, sizeof(sRawMap));
	RemoveMapPath(sRawMap, sMap, sizeof(sMap));
	
	char sPath[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, sPath, sizeof(sPath), "configs/hg/knife_arena/%s.txt", sMap);
	
	if(FileExists(sPath))
		DeleteFile(sPath);
		
	Handle hFile = OpenFile(sPath, "w");
	
	if (hFile != INVALID_HANDLE)
	{
		WriteFileLine(hFile, "%.2f;%.2f;%.2f;%.2f;", g_fArenaCenter[0], g_fArenaCenter[1], g_fArenaCenter[2], g_fArenaStartRadius);
		CloseHandle(hFile);
		
		return true;
	}
	
	return false;
}

public Action Cmd_Remove_KnifeArena(int client, int args)
{
	if(client == 0)
		return Plugin_Handled;
		
	GetClientAbsOrigin(client, g_fArenaCenter);
	
	char sCurrentMap[128];
	GetCurrentMap(sCurrentMap, sizeof(sCurrentMap));
	
	char sPath[256];
	BuildPath(Path_SM, sPath, 256, "configs/hg/knife_arena/%s.txt", sCurrentMap);
	
	if(FileExists(sPath) && DeleteFile(sPath))
		CPrintToChat(client, "%s Knife arena center has been removed.", g_sPrefix);
	else CPrintToChat(client, "%s Knife arena center was not set.", g_sPrefix);
	
	return Plugin_Handled;
}

void CreateAreneaBeams(bool preview = false)
{
	// Create Arena
	float fArena[3];
	AddVectors(fArena, g_fArenaCenter, fArena);
	fArena[2] -= 128.0;
	
	for (int x = 0; x < 5; x++)
	{
		if(g_fArenaRadius <= 0.0)
			break;
			
		fArena[2] += 64.0;
		
		if(preview)
			TE_SetupBeamRingPoint(fArena, g_fArenaRadius, g_fArenaRadius+0.1, g_iLaser, g_iHalo, 0, 10, 0.1, 5.0, 0.0, {255, 100, 0, 255}, 1, 0);
		else TE_SetupBeamRingPoint(fArena, g_fArenaRadius, g_fArenaRadius+0.1, g_iLaser, g_iHalo, 0, 10, g_fArenaInterval, 5.0, 0.0, {255, 100, 0, 255}, 1, 0);
		
		TE_SendToAll();
	}
}