// Credits: tilastokeskus

public Action Cmd_spec_next(int client, const char[] command, int argc)
{
	return SpecNext(client);
}

public Action SpecNext(int client)
{
	if (client == 0 || !IsClientInGame(client) || IsPlayerAlive(client))
		return Plugin_Handled;
	
	int target = GetEntPropEnt(client, Prop_Send, "m_hObserverTarget");
	int nextTarget = GetNextClient(target, true);
	
	if (MaxClients > nextTarget > 0)
		SetEntPropEnt(client, Prop_Send, "m_hObserverTarget", nextTarget);
	
	return Plugin_Handled;
}

public Action Cmd_spec_prev(int client, const char[] command, int argc)
{
	return SpecPrev(client);
}

public Action SpecPrev(int client)
{
	if (client == 0 || !IsClientInGame(client) || IsPlayerAlive(client))
		return Plugin_Handled;
	
	int target = GetEntPropEnt(client, Prop_Send, "m_hObserverTarget");
	int nextTarget = GetNextClient(target, false);
	
	if (nextTarget > 0)
		SetEntPropEnt(client, Prop_Send, "m_hObserverTarget", nextTarget);
	
	return Plugin_Handled;
}

public Action Cmd_spec_player(int client, const char[] command, int argc)
{
	if (client == 0 || !IsClientInGame(client) || IsPlayerAlive(client))
		return Plugin_Handled;
	
	char arg[128];
	GetCmdArg(1, arg, sizeof(arg));
	if (arg[0])
	{
		char targetName[128];
		int targets[MAXPLAYERS];
		bool tn_is_ml;
		int numTargets = ProcessTargetString(arg, client, targets, MaxClients, COMMAND_FILTER_CONNECTED, targetName, sizeof(targetName), tn_is_ml);
		
		if (numTargets < 1)
			return SpecNext(client);
		
		int target = targets[GetRandomInt(0, numTargets - 1)];
		
		if(target < 1 || !IsClientInGame(target) || !IsPlayerAlive(target))
			return SpecNext(client);
		
		SetEntPropEnt(client, Prop_Send, "m_hObserverTarget", target);
	}
	
	return Plugin_Handled;
}

public Action Cmd_spec_mode(int client, const char[] command, int argc)
{
	if (client == 0 || !IsClientInGame(client) || IsPlayerAlive(client))
		return Plugin_Handled;
	
	int iMode = 1 + GetEntProp(client, Prop_Send, "m_iObserverMode");
	
	if (iMode > SPECMODE_FREELOOK)
		iMode = SPECMODE_FIRSTPERSON;
		
	SetEntProp(client, Prop_Send, "m_iObserverMode", iMode);
	
	return Plugin_Handled;
}

public Action Timer_SetObserv(Handle timer, int client)
{
	if (IsClientInGame(client) && !IsPlayerAlive(client))
	{
		int target = GetEntPropEnt(client, Prop_Send, "m_hObserverTarget");
		
		if (target == -1)
			target = client;
		
		int nextTarget = GetNextClient(target, true);
		if (nextTarget != -1)
			SetEntPropEnt(client, Prop_Send, "m_hObserverTarget", nextTarget);
	
		CreateTimer(0.1, Timer_SetMode, client);
	}
}

// make any players observing a dead CT observe another CT 
public Action Timer_CheckObservers(Handle timer, int client)
{
	if (IsClientInGame(client) && !IsPlayerAlive(client))
	{
		LoopIngameClients(i)
		{
			if (!IsPlayerAlive(i) && i != client)
			{
				// who this player is observing now
				int target = GetEntPropEnt(i, Prop_Send, "m_hObserverTarget");
				if (target == client)
				{
					// if it's the dead player, pick a int target
					int nextTarget = GetNextClient(client, true);
					if (nextTarget > 0)
						SetEntPropEnt(i, Prop_Send, "m_hObserverTarget", nextTarget);
				}
			}
		}
	}
}

public Action Timer_SetMode(Handle timer, int client)
{
	if (IsClientConnected(client) && IsClientInGame(client) && !IsPlayerAlive(client))
	{
		int iMode = 1 + GetEntProp(client, Prop_Send, "m_iObserverMode");
		
		if (iMode > SPECMODE_FREELOOK)
			iMode = SPECMODE_FIRSTPERSON;
			
		SetEntProp(client, Prop_Send, "m_iObserverMode", iMode);
	}
}