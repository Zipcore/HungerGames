Handle g_hSurvivalTimer = null;

float g_fHunger[MAXPLAYERS+1] = {100.0, ...};
float g_fEnergy[MAXPLAYERS+1] = {100.0, ...};
float g_fThirst[MAXPLAYERS+1] = {100.0, ...};
float g_fStamina[MAXPLAYERS+1] = {100.0, ...};

float g_fHealthBuffer[MAXPLAYERS + 1];
float g_fStaminaBuffer[MAXPLAYERS+1] ;

void StartSurvivalTimer()
{
	if(g_hSurvivalTimer != null)
		CloseHandle(g_hSurvivalTimer);
	
	g_hSurvivalTimer = CreateTimer(g_fSurvivalCheckInterval, Timer_CheckSurvivalPlayers, 0, TIMER_REPEAT);
}

public Action Timer_CheckSurvivalPlayers(Handle timer, any data)
{
	CheckSurvivalPlayers();
	
	return Plugin_Continue;
}

void CheckSurvivalPlayers()
{
	if(RS_InProgress > RoundStatus > RS_Endgame || IsFinal() || !g_bSurvival)
		return;
	
	LoopIngameClients(client)
	{
		if(!IsPlayerAlive(client))
		{
			//PerformBlind(client);
			continue;
		}
		
		if(!g_bIsTribute[client])
		{
			//PerformBlind(client);
			continue;
		}
			
		CheckSurvivalPlayer(client);
	}
}

void CheckSurvivalPlayer(int client)
{	
	float fVelocity[3];
	GetEntPropVector(client, Prop_Data, "m_vecVelocity", fVelocity);
	float currentspeed = SquareRoot(Pow(fVelocity[0], 2.0) + Pow(fVelocity[1], 2.0));
	
	// Consume Energy
	if(g_fHunger[client] > 0 && g_fEnergy[client] < 100.0)
	{
		if(currentspeed > g_fSpeedRunning) 
		{
			g_fHunger[client] -= g_fHungerRunning;
			g_fEnergy[client] += g_fHungerRunning;
		}
		else if(currentspeed > g_fSpeedWalking) 
		{
			g_fHunger[client] -= g_fHungerWalking;
			g_fEnergy[client] += g_fHungerWalking;
		}
		else
		{
			g_fHunger[client] -= g_fHungerCamping;
			g_fEnergy[client] += g_fHungerCamping;
		}
		
		if(g_fEnergy[client] > 100.0)
			g_fEnergy[client] = 100.0;
		
		if(g_fHunger[client] < 0.0)
			g_fHunger[client] = 0.0;
	}
	// Loose Energy
	else 
	{
		if(currentspeed > g_fSpeedRunning) 
			g_fEnergy[client] -= g_fStarveRunning;
		else if(currentspeed > g_fSpeedRunning) 
			g_fEnergy[client] -= g_fStarveWalking;
		else g_fEnergy[client] -= g_fStarveCamping;
		
		if(g_fEnergy[client] < 0.0)
			g_fEnergy[client] = 0.0;
	}
	
	// Starving
	
	if(g_fHealthStarvingPassage >= 0.0 && g_fHunger[client]+g_fEnergy[client] <= g_fHealthStarvingPassage)
	{
		if(g_fHealthBuffer[client] > 0.0)
			g_fHealthBuffer[client] = 0.0;
		
		g_fHealthBuffer[client] -= g_fHealthLose;
		
		while(g_fHealthBuffer[client] < -1.0)
		{
			int health = GetClientHealth(client);
			if(health > g_iHealthMin)
			{
				SetEntityHealth(client, GetClientHealth(client)-1);
				g_fHealthBuffer[client] += 1.0;
			}
			else 
			{
				g_fHealthBuffer[client] = 0.0;
				
				if(g_iHealthMin <= 0)
				{
					ForcePlayerSuicide(client);
					CPrintToChat(client, "%T", "Death_Starving", client, g_sPrefix);
				}
			}
		}
	}
	
	/* Regen Life */
	
	else if(g_fHealthRecoveryPassage >= 0.0 && g_fHunger[client]+g_fEnergy[client] > g_fHealthRecoveryPassage && GetClientHealth(client) < g_iHealthMax)
	{
		if(g_fHealthBuffer[client] < 0.0)
			g_fHealthBuffer[client] = 0.0;
		
		g_fHealthBuffer[client] += g_fHealthGain;
		g_fHunger[client] -= g_fHealthRecoveryCost;
		
		while(g_fHealthBuffer[client] > 1.0)
		{
			int health = GetClientHealth(client);
			if(health < g_iHealthMax)
			{
				SetEntityHealth(client, GetClientHealth(client)+1);
				g_fHealthBuffer[client] -= 1.0;
			}
			else g_fHealthBuffer[client] = 0.0;
		}
	}
	
	/* Stamina */
		
	float eRegenRate = ( g_fThirst[client] * g_fStaminaRecoverThirstPerc ) + ( g_fStamina[client] * g_fStaminaRecoverBasePerc );
	
	// Player is running
	if(currentspeed > g_fSpeedRunning && !HasParachuteEnabled(client))
	{
		g_fStamina[client] -= g_fStaminaRunningCost;
		g_fThirst[client] -= g_fThirstRunning;
	}
	
	// Player is walking or camping
	else
	{
		g_fStamina[client] += ( eRegenRate + g_fStaminaRecoverBase );
		
		// Don't recover more than 100%
		if(g_fStamina[client] > 100.0)
			g_fStamina[client] = 100.0;
		
		if(currentspeed > g_fSpeedWalking)
			g_fThirst[client] -= g_fThirstWalking;
		else g_fThirst[client] -= g_fThirstCamping;
	}
	
	/* Stamina buffer */
	
	if(g_fStamina[client] < 100.0 && g_fStaminaBuffer[client] > 0.0)
	{
		// Base recovery
		float fStaminaToAdd = g_fStaminaRecoverBuffer;
		
		// Stamina buffer is almost empty
		if(fStaminaToAdd > g_fStaminaBuffer[client])
			fStaminaToAdd = g_fStaminaBuffer[client];
		
		// Stamina is almost not exhausted
		if(fStaminaToAdd + g_fStamina[client] > 100.0)
			fStaminaToAdd = 100.0-g_fStamina[client];
		
		// If our player is vip boost his recovery rate
		if(Client_HasAdminFlags(client, g_iVIPflags))
			fStaminaToAdd *= g_fStaminaRecoverVIP;
		
		// Is there still a need to use the stamina buffer
		if(fStaminaToAdd > 0.0)
		{
			g_fStamina[client] += fStaminaToAdd;
			g_fStaminaBuffer[client] -= fStaminaToAdd;
		}
	}
	
	/* Drink from water */
	
	if (g_fThirstWaterDrinkRate > 0 && 2 >= GetEntProp(client, Prop_Data, "m_nWaterLevel") >= 1 && currentspeed < g_fSpeedWalking)
	{
		if(g_fThirst[client] < 100.0) 
			g_fThirst[client] += g_fThirstWaterDrinkRate;
	}
	
	// Check limits
	
	if(g_fThirst[client] > 100.0)
		g_fThirst[client] = 100.0;
	
	if(g_fThirst[client] < 0.0)
		g_fThirst[client] = 0.0;
	
	if(g_fStamina[client] > 100.0)
	{
		g_fStaminaBuffer[client] += g_fStamina[client] - 100.0;
		g_fStamina[client] = 100.0;
	}
	
	if(g_fStamina[client] < 0.0)
		g_fStamina[client] = 0.0;
	
	if(g_fHunger[client] > 100.0)
		g_fHunger[client] = 100.0;
	
	if(g_fHunger[client] < 0.0)
		g_fHunger[client] = 0.0;
	
	if(g_fEnergy[client] > 100.0)
		g_fEnergy[client] = 100.0;
	
	if(g_fEnergy[client] < 0.0)
		g_fEnergy[client] = 0.0;
	
	// Limit stamina to energy status
	
	if(g_fStamina[client] > g_fEnergy[client])
		g_fStamina[client] = g_fEnergy[client];
		
	UpdateSpeed(client);
}

void UpdateSpeed(int client)
{
	if(!g_bIsTribute[client])
		SetEntPropFloat(client, Prop_Data, "m_flLaggedMovementValue", 1.0);
	else if(g_fStamina[client] > 75.0)
		SetEntPropFloat(client, Prop_Data, "m_flLaggedMovementValue", g_fStaminaPerfectSpeed);
	else if(g_fStamina[client] < 1.0)
		SetEntPropFloat(client, Prop_Data, "m_flLaggedMovementValue", g_fStaminaEmptySpeed);
	else if(g_fStamina[client] < 15.0)
		SetEntPropFloat(client, Prop_Data, "m_flLaggedMovementValue", g_fStaminaVerySpeed);
	else if(g_fStamina[client] < 35.0)
		SetEntPropFloat(client, Prop_Data, "m_flLaggedMovementValue", g_fStaminaLowSpeed);
	else SetEntPropFloat(client, Prop_Data, "m_flLaggedMovementValue", g_fStaminaGoodSpeed);
}

/* Natives */

public int Native_GetHunger(Handle plugin, int numParams)
{
	int client = GetNativeCell(1);
	SetNativeCellRef(2, g_fHunger[client]);
}

public int Native_SetHunger(Handle plugin, int numParams)
{
	int client = GetNativeCell(1);
	float value = GetNativeCell(2);
	
	g_fHunger[client] = value;
	
	if(g_fHunger[client] > 100.0)
		g_fHunger[client] = 100.0;
	
	if(g_fHunger[client] < 0.0)
		g_fHunger[client] = 0.0;
}

public int Native_AddHunger(Handle plugin, int numParams)
{
	int client = GetNativeCell(1);
	float value = GetNativeCell(2);
	
	g_fHunger[client] += value;
	
	if(g_fHunger[client] > 100.0)
		g_fHunger[client] = 100.0;
	
	if(g_fHunger[client] < 0.0)
		g_fHunger[client] = 0.0;
}

public int Native_ResetHunger(Handle plugin, int numParams)
{
	int client = GetNativeCell(1);
	g_fHunger[client] = g_fHungerDefault;
	g_fHealthBuffer[client] = 0.0;
}

public int Native_GetEnergy(Handle plugin, int numParams)
{
	int client = GetNativeCell(1);
	SetNativeCellRef(2, g_fEnergy[client]);
}

public int Native_SetEnergy(Handle plugin, int numParams)
{
	int client = GetNativeCell(1);
	float value = GetNativeCell(2);
	
	g_fEnergy[client] = value;
	
	if(g_fEnergy[client] > 100.0)
		g_fEnergy[client] = 100.0;
	
	if(g_fEnergy[client] < 0.0)
		g_fEnergy[client] = 0.0;
}

public int Native_AddEnergy(Handle plugin, int numParams)
{
	int client = GetNativeCell(1);
	float value = GetNativeCell(2);
	
	g_fEnergy[client] += value;
	
	if(g_fEnergy[client] > 100.0)
		g_fEnergy[client] = 100.0;
	
	if(g_fEnergy[client] < 0.0)
		g_fEnergy[client] = 0.0;
}

public int Native_ResetEnergy(Handle plugin, int numParams)
{
	int client = GetNativeCell(1);
	g_fEnergy[client] = g_fEnergyDefault;
	g_fHealthBuffer[client] = 0.0;
}

public int Native_GetThirst(Handle plugin, int numParams)
{
	int client = GetNativeCell(1);
	SetNativeCellRef(2, g_fThirst[client]);
}

public int Native_SetThirst(Handle plugin, int numParams)
{
	int client = GetNativeCell(1);
	float value = GetNativeCell(2);
	
	g_fThirst[client] = value;
	
	if(g_fThirst[client] > 100.0)
		g_fThirst[client] = 100.0;
	
	if(g_fThirst[client] < 0.0)
		g_fThirst[client] = 0.0;
}

public int Native_AddThirst(Handle plugin, int numParams)
{
	int client = GetNativeCell(1);
	float value = GetNativeCell(2);
	
	g_fThirst[client] += value;
	
	if(g_fThirst[client] > 100.0)
		g_fThirst[client] = 100.0;
	
	if(g_fThirst[client] < 0.0)
		g_fThirst[client] = 0.0;
}

public int Native_ResetThirst(Handle plugin, int numParams)
{
	int client = GetNativeCell(1);
	g_fThirst[client] = g_fThirstDefault;
}

public int Native_GetStamina(Handle plugin, int numParams)
{
	int client = GetNativeCell(1);
	SetNativeCellRef(2, g_fStamina[client]);
}

public int Native_SetStamina(Handle plugin, int numParams)
{
	int client = GetNativeCell(1);
	float value = GetNativeCell(2);
	
	g_fStamina[client] = value;
	
	if(g_fStamina[client] > 100.0)
	{
		g_fStaminaBuffer[client] += g_fStamina[client] - 100.0;
		g_fStamina[client] = 100.0;
	}
	
	if(g_fStamina[client] < 0.0)
		g_fStamina[client] = 0.0;
}

public int Native_AddStamina(Handle plugin, int numParams)
{
	int client = GetNativeCell(1);
	float value = GetNativeCell(2);
	
	g_fStamina[client] += value;
	
	if(g_fStamina[client] > 100.0)
	{
		g_fStaminaBuffer[client] += g_fStamina[client] - 100.0;
		g_fStamina[client] = 100.0;
	}
	
	if(g_fStamina[client] < 0.0)
		g_fStamina[client] = 0.0;
}

public int Native_ResetStamina(Handle plugin, int numParams)
{
	int client = GetNativeCell(1);
	g_fStamina[client] = g_fStaminaDefault;
	g_fStaminaBuffer[client] = 0.0;
}