char g_sWeapons[][] = {
	"weapon_ak47", "weapon_aug", "weapon_bizon", "weapon_cz75a", "weapon_deagle", "weapon_decoy", "weapon_elite", "weapon_famas", "weapon_fiveseven", "weapon_flashbang", 
	"weapon_g3sg1", "weapon_galilar", "weapon_glock", "weapon_hegrenade", "weapon_hkp2000", "weapon_incgrenade", "weapon_knife", "weapon_m249", "weapon_m4a1", "weapon_awp",
	"weapon_m4a1_silencer", "weapon_mac10", "weapon_mag7", "weapon_molotov", "weapon_mp7", "weapon_mp9", "weapon_negev", "weapon_nova", "weapon_p250", "weapon_p90", "weapon_revolver",
	"weapon_sawedoff", "weapon_scar20", "weapon_sg556", "weapon_smokegrenade", "weapon_ssg08", "weapon_taser", "weapon_tec9", "weapon_ump45", "weapon_usp_silencer", "weapon_xm1014",
	"weapon_healthshot", "weapon_tagrenade"
};

int g_sWeaponSlots[] = {
	CS_SLOT_PRIMARY, CS_SLOT_PRIMARY, CS_SLOT_PRIMARY, CS_SLOT_SECONDARY, CS_SLOT_SECONDARY, CS_SLOT_GRENADE, CS_SLOT_SECONDARY, CS_SLOT_PRIMARY, CS_SLOT_SECONDARY, CS_SLOT_GRENADE, 
	CS_SLOT_PRIMARY, CS_SLOT_PRIMARY, CS_SLOT_SECONDARY, CS_SLOT_GRENADE, CS_SLOT_SECONDARY, CS_SLOT_GRENADE, CS_SLOT_KNIFE, CS_SLOT_PRIMARY, CS_SLOT_PRIMARY, CS_SLOT_PRIMARY,
	CS_SLOT_PRIMARY, CS_SLOT_PRIMARY, CS_SLOT_PRIMARY, CS_SLOT_GRENADE, CS_SLOT_PRIMARY, CS_SLOT_PRIMARY, CS_SLOT_PRIMARY, CS_SLOT_PRIMARY, CS_SLOT_SECONDARY, CS_SLOT_PRIMARY, CS_SLOT_SECONDARY,
	CS_SLOT_PRIMARY, CS_SLOT_PRIMARY, CS_SLOT_PRIMARY, CS_SLOT_GRENADE, CS_SLOT_PRIMARY, CS_SLOT_KNIFE, CS_SLOT_SECONDARY, CS_SLOT_PRIMARY, CS_SLOT_SECONDARY, CS_SLOT_PRIMARY,
	CS_SLOT_C4, CS_SLOT_GRENADE
};

#define MAXWEAPONS sizeof(g_sWeapons)

Handle g_aLootTableNames = null;
Handle g_aLootTableWeapon[128] = {null, ...};

Handle g_aLootTableWeaponAmmoMin[128] = {null, ...};
Handle g_aLootTableWeaponAmmoMax[128] = {null, ...};

Handle g_aLootTableWeaponReserveAmmoMin[128] = {null, ...};
Handle g_aLootTableWeaponReserveAmmoMax[128] = {null, ...};

int LoadLootTable(const char[] sName)
{
	if(strlen(sName) <= 0)
		return -1;
	
	int iIndex = -1;
	
	if(g_aLootTableNames == null)
	{
		g_aLootTableNames = CreateArray(32);
		iIndex = PushArrayString(g_aLootTableNames, sName);
	}
	else
	{
		int iCount = GetArraySize(g_aLootTableNames);
		
		for (int i = 0; i < iCount; i++)
		{
			char sBuffer[32];
			GetArrayString(g_aLootTableNames, i, sBuffer, sizeof(sBuffer));
			
			if(StrEqual(sName, sBuffer))
			{
				iIndex = i;
				break;
			}
		}
	}
	
	if (iIndex == -1)
		iIndex = GetArraySize(g_aLootTableNames);
	
	char sPath[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, sPath, sizeof(sPath), "configs/loottables/%s.txt", sName);
	
	if(!FileExists(sPath))
	{
		LogError("Can't load loottable, config not found: %s.", sPath);
		return -1;
	}
	
	Handle hFile = OpenFile(sPath, "r");
	if (hFile == null)
	{
		LogError("Can't load loottable, config not accessible: %s.", sPath);
		return -1;
	}
	
	char sBuffer[512];
	char datas[6][32];
	
	if(g_aLootTableWeapon[iIndex] == null)
		g_aLootTableWeapon[iIndex] = CreateArray(32);
	else ClearArray(g_aLootTableWeapon[iIndex]);
	
	if(g_aLootTableWeaponAmmoMin[iIndex] == null)
		g_aLootTableWeaponAmmoMin[iIndex] = CreateArray(1);
	else ClearArray(g_aLootTableWeaponAmmoMin[iIndex]);
	
	if(g_aLootTableWeaponAmmoMax[iIndex] == null)
		g_aLootTableWeaponAmmoMax[iIndex] = CreateArray(1);
	else ClearArray(g_aLootTableWeaponAmmoMax[iIndex]);
	
	if(g_aLootTableWeaponReserveAmmoMin[iIndex] == null)
		g_aLootTableWeaponReserveAmmoMin[iIndex] = CreateArray(1);
	else ClearArray(g_aLootTableWeaponReserveAmmoMin[iIndex]);
	
	if(g_aLootTableWeaponReserveAmmoMax[iIndex] == null)
		g_aLootTableWeaponReserveAmmoMax[iIndex] = CreateArray(1);
	else ClearArray(g_aLootTableWeaponReserveAmmoMax[iIndex]);
	
	while (ReadFileLine(hFile, sBuffer, sizeof(sBuffer))) 
	{
		if(StrContains(sBuffer, "//") != -1)
			continue;
		
		TrimString(sBuffer);
		ExplodeString(sBuffer, ";", datas, sizeof(datas), sizeof(sBuffer));
		
		if(!StrEqual(datas[0], ""))
		{
			int iWeight = StringToInt(datas[1]);
			
			int iAmmoMin = StringToInt(datas[2]);
			int iAmmoMax = StringToInt(datas[3]);
			
			int iReserveAmmoMin = StringToInt(datas[4]);
			int iReserveAmmoMax = StringToInt(datas[5]);
			
			for(int x = 0; x < iWeight; x++)
			{
				PushArrayString(g_aLootTableWeapon[iIndex], datas[0]);
				PushArrayCell(g_aLootTableWeaponAmmoMin[iIndex], iAmmoMin);
				PushArrayCell(g_aLootTableWeaponAmmoMax[iIndex], iAmmoMax);
				PushArrayCell(g_aLootTableWeaponReserveAmmoMin[iIndex], iReserveAmmoMin);
				PushArrayCell(g_aLootTableWeaponReserveAmmoMax[iIndex], iReserveAmmoMax);
			}
		}
	}
	
	CloseHandle(hFile);
	
	return iIndex;
}

int GetLootTableIndex(const char[] sName)
{
	if(strlen(sName) <= 0)
		return -1;
	
	int iCount = GetArraySize(g_aLootTableNames);
	
	if(iCount <= 0)
		return -1;
	
	for (int i = 0; i < iCount; i++)
	{
		char sBuffer[32];
		GetArrayString(g_aLootTableNames, i, sBuffer, sizeof(sBuffer));
		
		if(StrEqual(sName, sBuffer))
			return i;
	}
	
	int index = LoadLootTable(sName);
	
	return index;
}

bool GiveRandomWeapon(int iClient, const char[] sName, char sWeapon[32])
{
	int iLootTable = GetLootTableIndex(sName);
	
	if(iLootTable == -1)
		return false;
	
	int size = GetArraySize(g_aLootTableWeapon[iLootTable]);
	
	if(size < 1)
		return false;
	
	int iRandom = GetRandomInt(0, size - 1);
	GetArrayString(g_aLootTableWeapon[iLootTable], iRandom, sWeapon, 32);
	
	// Yep we allow empty crates
	if(StrContains(sWeapon, "nothing") != -1)
		return true;
	
	int iCurrentSlot = GetSlotsByWeaponType(g_iWepType[iClient]);
	
	for(int i = 0; i < MAXWEAPONS; i++)
	{
		if(StrContains(g_sWeapons[i], sWeapon) == -1)
			continue;
		
		if(StrContains(sWeapon, "knife") != -1)
		{
			bool bHasTaser;
			bool bHasKnife;
			int iWeapon = GetPlayerWeaponSlot(iClient, CS_SLOT_KNIFE);
			
			while (iWeapon != -1)
			{
				// check for taser
				
				if (GetEntProp(iWeapon, Prop_Send, "m_iItemDefinitionIndex") == 31)
					bHasTaser = true;
				else bHasKnife = true;
				
				RemovePlayerItem(iClient, iWeapon);
				AcceptEntityInput(iWeapon, "Kill");
				
				iWeapon = GetPlayerWeaponSlot(iClient, CS_SLOT_KNIFE);
			}
			
			if (bHasKnife)
			{
				AddThrowingKnife(iClient);
				Format(sWeapon, sizeof(sWeapon), "%T", "throwingknife", iClient);
			}
			else Format(sWeapon, sizeof(sWeapon), "%T", "knife", iClient);
			
			GivePlayerItem(iClient, "weapon_knife");
			
			// Give back taser
			
			if (bHasTaser)
				GivePlayerItem(iClient, "weapon_taser");
			
			SwitchToSlotDelayed(iClient, iCurrentSlot, 0.0);
			
			return true;
		}
		
		// Translate the weapon name
		
		Format(sWeapon, sizeof(sWeapon), "%T", sWeapon, iClient);
		
		// Ignore primary and secondary weapons
		
		int iSlot = g_sWeaponSlots[i];
		if (iSlot != CS_SLOT_PRIMARY && iSlot != CS_SLOT_SECONDARY)
		{
			GivePlayerItem(iClient, g_sWeapons[i]);
			SwitchToSlotDelayed(iClient, iCurrentSlot, 0.0);
			return true;
		}
		
		// Drop weapon from this slot
		
		int iCurrentWeapon = GetPlayerWeaponSlot(iClient, iSlot);
		if(iCurrentWeapon != -1)
		{
			float fNull[3];
			SDKHooks_DropWeapon(iClient, iCurrentWeapon, NULL_VECTOR, fNull);
		}
		
		// Give new weapon and set it's reserve ammo
		
		int iWeapon = GivePlayerItem(iClient, g_sWeapons[i]);
		EquipPlayerWeapon(iClient, iWeapon);
	
		// Clip ammo
		int iAmmoMin = GetArrayCell(g_aLootTableWeaponAmmoMin[iLootTable], iRandom);
		int iAmmoMax = GetArrayCell(g_aLootTableWeaponAmmoMax[iLootTable], iRandom);
		
		int iAmmo = -1;
		if(iAmmoMin != -1 && iAmmoMin != -1)
			iAmmo = GetRandomInt(iAmmoMin, iAmmoMax);
		else if(iAmmoMin != -1)
			iAmmo = iAmmoMin;
		else if(iAmmoMax != -1)
			iAmmo = iAmmoMax;
		
		if(iAmmo >= 0)
			Weapon_SetAmmo(iWeapon, iAmmo);
		
		// Reserve ammo
		int iReserveAmmoMin = GetArrayCell(g_aLootTableWeaponReserveAmmoMin[iLootTable], iRandom);
		int iReserveAmmoMax = GetArrayCell(g_aLootTableWeaponReserveAmmoMax[iLootTable], iRandom);
		
		int iReserveAmmo = -1;
		if(iReserveAmmoMin != -1 && iReserveAmmoMin != -1)
			iReserveAmmo = GetRandomInt(iReserveAmmoMin, iReserveAmmoMax);
		else if(iReserveAmmoMin != -1)
			iReserveAmmo = iReserveAmmoMin;
		else if(iReserveAmmoMax != -1)
			iReserveAmmo = iReserveAmmoMax;
		
		if(iReserveAmmo >= 0)
			Weapon_SetReserveAmmo(iWeapon, iReserveAmmo);
		
		SwitchToSlotDelayed(iClient, iCurrentSlot, 0.0);
		
		return true;
	}
	
	return false;
}

stock void Weapon_SetAmmo(int iWeapon, int iAmmo)
{
	SetEntProp(iWeapon, Prop_Data, "m_iClip1", iAmmo);
}

stock void Weapon_SetReserveAmmo(int iWeapon, int iReserveAmmo)
{
	SetEntProp(iWeapon, Prop_Send, "m_iPrimaryReserveAmmoCount", iReserveAmmo);
}

stock void Weapon_AddReserveAmmo(int iWeapon, int iAddAmmo)
{
	int iAmmo = GetEntProp(iWeapon, Prop_Send, "m_iPrimaryReserveAmmoCount");
	iAmmo += iAddAmmo;
	SetEntProp(iWeapon, Prop_Send, "m_iPrimaryReserveAmmoCount", iAmmo);
}