int g_iChargesLeft;

void StartChargedWater()
{
	g_iChargesLeft = g_iEventChargedWaterSimulationsMax;
	CreateTimer(g_fEventChargedWaterInterval, Timer_SimulateChargedWater, _, TIMER_FLAG_NO_MAPCHANGE);
}

public Action Timer_SimulateChargedWater(Handle timer, any data)
{
	if(RoundStatus != RS_InProgress && !(g_bEventEndgame && RoundStatus == RS_Endgame))
		return Plugin_Stop;
		
	g_iChargesLeft--;
		
	if(g_iChargesLeft > 0)
	{
		SimulateChargedWater();
		CreateTimer(g_fEventChargedWaterInterval, Timer_SimulateChargedWater, _, TIMER_FLAG_NO_MAPCHANGE);
	}
	
	return Plugin_Stop;
}

void SimulateChargedWater()
{
	float fTop[3];
	float fMax[3];
	float fMin[3];
	
	GetWorldMax(fMin, fMax, fTop);
	
	float fPos[3];
	float fNormal[3];
	
	float tempPos[3];
	tempPos[2] = fTop[2];
	
	int iWaterSpots;
	for(int i = 0; i < 128; i++)
	{
		fPos[0] = GetRandomFloat(fMax[0], fMin[0]);
		fPos[1] = GetRandomFloat(fMax[1], fMin[1]);
		fPos[2] = fTop[2];
			
		float fPosWater[3];
		TraceWater(fPos, AngleDown, fPosWater, fNormal);
		
		if(fPosWater[0] != 0.0 && fPosWater[1] != 0.0 && fPosWater[2] != 0.0)
		{
			iWaterSpots++;
			
			SpawnTesla(fPosWater);
			
			if(iWaterSpots >= 10)
				break;
		}
	}
	
	LoopAlivePlayers(i)
	{
		if(GetEntProp(i, Prop_Data, "m_nWaterLevel") > 0)
			Point_Hurt(i, g_iEventChargedWaterDamage);
	}
}