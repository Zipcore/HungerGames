/* Settings */

float g_fInfernoHeightMin = 256.0;
float g_fInfernoHeightMax = 2048.0;
float g_fInfernoHeightDefault = 512.0;

int g_iInfernosLeft;
float g_iInfernoPos[3];

int g_iNapalmTarget;

void StartNapalmAirStrike(int target)
{
	g_iNapalmTarget = target;
	
	g_iInfernosLeft = 0;
	
	int iAlive = 0;
	LoopAlivePlayers(i)
		iAlive++;
	
	if(iAlive > 0)
	{
		float distance;
		GetRoofDistance(g_iNapalmTarget, distance);
		
		if(distance > g_fInfernoHeightMin)
		{
			GetClientAbsOrigin(g_iNapalmTarget, g_iInfernoPos);
			
			if(distance > g_fInfernoHeightMax)
				distance = g_fInfernoHeightMax;
		}
		else distance = g_fInfernoHeightDefault;
				
		g_iInfernoPos[2] += distance;
		
		g_iInfernosLeft = g_iEventNapalmProjectiles;
		
		CreateTimer(0.0, Timer_Inferno, _, TIMER_FLAG_NO_MAPCHANGE);
	}
}

public Action Timer_Inferno(Handle timer, any data)
{
	if(RoundStatus != RS_InProgress && !(g_bEventEndgame && RoundStatus == RS_Endgame))
		return Plugin_Stop;
	
	g_iInfernosLeft--;
	
	CreateInferno();
	
	if(g_iInfernosLeft > 0)
		CreateTimer(0.3, Timer_Inferno, _, TIMER_FLAG_NO_MAPCHANGE);
	
	return Plugin_Stop;
}

void CreateInferno()
{
	int iEntity = CreateEntityByName("molotov_projectile");
	if(iEntity == INVALID_ENT_REFERENCE)
		return;
		
	float pos[3];
	pos[0] = g_iInfernoPos[0] + GetRandomFloat(-g_fEventNapalmRadius, g_fEventNapalmRadius);
	pos[1] = g_iInfernoPos[1] + GetRandomFloat(-g_fEventNapalmRadius, g_fEventNapalmRadius);
	pos[2] = g_iInfernoPos[2];

	SetEntPropEnt(iEntity, Prop_Send, "m_hThrower", 0);
	SetEntPropEnt(iEntity, Prop_Send, "m_hOwnerEntity", 0);
	SetEntProp(iEntity, Prop_Send, "m_iTeamNum", 2);
	SetEntPropFloat(iEntity, Prop_Send, "m_DmgRadius", 300.0);
	SetEntPropFloat(iEntity, Prop_Send, "m_flDamage", 200.0);
	SetEntityModel(iEntity, "models/weapons/w_eq_molotov_dropped.mdl");

	TeleportEntity(iEntity, pos, NULL_VECTOR, NULL_VECTOR);
	DispatchSpawn(iEntity);
}

public void OnMolotovProjectile(int iEntity)
{
	SDKUnhook(iEntity, SDKHook_Spawn, OnMolotovProjectile);
	
	int client = GetEntPropEnt(iEntity, Prop_Send, "m_hOwnerEntity");
	
	// Ignore entitys owned by a player
	if(client != 0)
		return;
		
	SDKHook(iEntity, SDKHook_StartTouch, StartTouchMolotov);
	
	int iRef = EntIndexToEntRef(iEntity);
	CreateTimer(0.0, Timer_DefuseMolotov, iRef);
	CreateTimer(0.0, Timer_ColorMolotov, iRef);
}

public Action StartTouchMolotov(int iEntity, int iEntity2)
{
	SDKUnhook(iEntity, SDKHook_StartTouch, StartTouchMolotov);
	
	int iRef = EntIndexToEntRef(iEntity);
	CreateTimer(0.0, Timer_DetonateMolotov, iRef);
}

public Action Timer_ColorMolotov(Handle timer, any ref)
{
	int ent = EntRefToEntIndex( ref );
	if ( ent != INVALID_ENT_REFERENCE )
	{
		TE_SetupBeamFollow(ent, g_iLaser, 0, 1.0, 3.0,3.0, 1, {255,155,10,240});
		TE_SendToAll();
	}
}

public Action Timer_DefuseMolotov(Handle timer, any ref)
{
	int ent = EntRefToEntIndex( ref );
	if ( ent != INVALID_ENT_REFERENCE )
		SetEntProp(ent, Prop_Data, "m_nNextThinkTick", -1);
}

public Action Timer_DetonateMolotov(Handle timer, any ref)
{
	int ent = EntRefToEntIndex( ref );
	if ( ent != INVALID_ENT_REFERENCE )
	{
		SetEntProp(ent, Prop_Data, "m_nNextThinkTick", 1);
		SetEntProp(ent, Prop_Data, "m_takedamage", 2 );
		SetEntProp(ent, Prop_Data, "m_iHealth", 1 );
		SDKHooks_TakeDamage(ent, 0, 0, 1.0);
	}
}