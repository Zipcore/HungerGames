char removeEntityList[][] = {
	"game_player_equip",
	"point_template",
	"func_breakable",
	"logic_case",
	"logic_timer",
	"math_counter",
	"point_servercommand",
	"filter_activator_name",
	"weapon_knife"
};

// Delete crates spawned by the map
void CleanMapCrates()
{
	if(!g_pZcoreLootSpawner)
		return;
	
	for (int i = 0; i < sizeof(removeEntityList); i++)
	{
		int entity = -1;
		while ((entity = FindEntityByClassname(entity, removeEntityList[i])) != INVALID_ENT_REFERENCE)
			AcceptEntityInput(entity, "Kill");
	}
}